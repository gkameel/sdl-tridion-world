﻿var fieldId = '';
var returnValue = '';
/**
* Initialize and load Item Selector iframe.
*/
function init() {
    //console.log("init!");
    if (window.location.search.length > 0 && window.location.search.indexOf("idonly") != -1) {
        idonly = true;
    }

    fieldId = getUrlParameter('fieldId');
//    console.log("fieldId: " + fieldId);

    var start = getUrlParameter('start');
//    console.log("start: " + start);

    var types = getUrlParameter('types');
//    console.log("types: " + types);

    returnValue = getUrlParameter('returnValue');
//    console.log("returnValue: " + returnValue);

    var url = 'ItemSelector.aspx?';
    if (start != undefined) {
        url = url + 'pubid=' + start + '&';
    }
    if (types != undefined) {
        url = url + 'types=' + types + '&';
    }
    if (returnValue == "uri" || returnValue == "title" || returnValue == "path" || returnValue == "webdavurl") {
        url = url + 'returnValue=obj&';
    }
    console.log("ItemSelector url: " + url);
    document.getElementById("selectorframe").src = url;
}

/**
* Set selected value back in field and close popup window.
*/
function setvalue(uri) {
    var value = uri;

    if (returnValue == "id") {
        // split tcmuri and use item id as value (tcm:2-3-4, item id = 3)
        var parts = uri.split("-");
        value = parts[1];
    }
    //console.log("Found Value: " + value);

    if (value.uri != undefined) {
        value.id = value.uri.split("-")[1];
        if (returnValue == "id") { value.requestedValue = value.id }
        if (returnValue == "uri") { value.requestedValue = value.uri }
        if (returnValue == "title") { value.requestedValue = value.title }
        if (returnValue == "path") { value.requestedValue = value.path }
        if (returnValue == "webdavurl") { value.requestedValue = value.webdavurl }
    }

    if (inIframe) {
        if (fieldId == undefined) {
            fieldId = "";
        }
        console.log("Calling function 'SetValueFromItemSelector' in parent, with parameters (value='" + value + "', id='" + fieldId + "')");
        if (parent.SetValueFromItemSelector != undefined)
        {
            parent.SetValueFromItemSelector(value, fieldId);
        }
        else {
            var val = (value.requestedValue == undefined) ? value : value.requestedValue;
            alert(val);
        }
    }
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}
