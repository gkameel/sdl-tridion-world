﻿function EnableItemSelectors() {
    // enable all itemSelectors on the page
    $("[data-itemselector-for]").click(function (event) {
        $('.tooltip').hide(); // hide tooltips
        ShowItemSelector(this);
        return false;
    });
}

// enable itemSelector for this element
function ShowItemSelector(e) {
    var href = e.href; // href on the element
    var fieldId = $(e).data("itemselector-for"); // ID of element to insert value in the HTML DOM (mandatory)
    var start = $(e).data("itemselector-start"); // start Uri (Publication uri, default: none)
    var types = $(e).data("itemselector-types"); // allowed itemtypes (int)
    var returnValue = $(e).data("itemselector-returnvalue"); // type of value expected from ItemSelector (uri, id, path, webdavurl)
    var height = $(e).data("itemselector-height"); // height of ItemSelector Popover
    if (height == undefined || height == "") {
        height = 400;
    }

    var url = '#urlMissing';
    if (href != undefined && href != "") {
        url = href + '?';
        if (fieldId != undefined && fieldId != "") {
            url = url + 'fieldId=' + fieldId + '&';
        }
        if (start != undefined && start != "") {
            url = url + 'start=' + start + '&';
        }
        if (types != undefined && types != "") {
            url = url + 'types=' + types + '&';
        }
        if (returnValue != undefined && returnValue != "") {
            url = url + 'returnValue=' + returnValue + '&';
        }
    }
    console.log("ItemSelector url:" + url);
    $(e).popover({
        content: '<iframe id="selectorframe" src="' + url + '" style="display:block;" height="' + height + '", width="400" frameBorder="0"></iframe>',
        html: true
    }).popover('toggle');
}

// this function gets called from the itemSelector PopOver (called from loaded iframe)
function SetValueFromItemSelector(value, fieldId) {
    console.log("SetValueFromItemSelector called with parameters (value = '" + value + "', fieldId = '" + fieldId + "')");

    var val = value;

    if (value.uri != undefined) {
        val = value.uri;
        console.log("value.requestedValue: " + value.requestedValue);
    }
    if (value.requestedValue != undefined) {
        val = value.requestedValue;
    }

    var target = $('#' + fieldId);

    if (target.prop("tagName").toUpperCase() == "SELECT") {
        target.append($("<option data-toggle=\"tooltip\"></option>").attr("value", val).attr("title", val).text(val));
    }

    else if ((target.prop("tagName").toUpperCase() == "INPUT")) {
        if (target.attr("type").toUpperCase() == "TEXT" || target.attr("type").toUpperCase() == "HIDDEN") {
            target.val(val);
        }
    }

    else {
        target.text(val);
    }

    // close all popovers
    $('[data-toggle="popover"]').popover("destroy");
    ActivateToolTips();
}

function ActivateToolTips() {
    $('[data-toggle="tooltip"][title!=""]').tooltip({ trigger: "manual", placement: 'auto', container: 'body', html: 'true' })
        .on("mouseenter", function () {
            var that = this;
            $(this).tooltip("show");
            $(".tooltip").on("mouseleave", function () {
                $(that).tooltip('hide');
            });
        }).on("mouseleave", function () {
            var that = this;
            setTimeout(function () {
                if (!$(".tooltip:hover").length) {
                    $(that).tooltip("hide");
                }
            }, 300);
        });
}

function DeSelectAllinList() {
    $("select.selectall option").prop("selected", false);
}

function SelectAllinList() {
    $("select.selectall option").prop("selected", true);
}

function ClosePopovers(e) {
    // Close all popovers when clicking outside an opened popover
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false  // fix for BS 3.3.6
        }
    });
}

$(document).ready(function () {
    EnableItemSelectors();
    ActivateToolTips();

    // hide popover when clicking outside of popover (not selecting a value in the ItemPicker
    $(document).on('click', function (e) { ClosePopovers(e); });

    // Show some results on submit
    $("button[type=submit]").click(function (event) {
        // Select all options in .selectall select boxes on the page before submit 
        //console.log("click submit! ");
        SelectAllinList();
        var msg = "";
        msg += "<p class=\"alert alert-success\">Single Uri: " + $("#singleUri").val() + "</p>";
        var urls = $("#multiWebDavUrls").val();
        //console.log("urls: " + urls);
        if (urls != undefined && urls.length > 0) {
            msg += "<p class=\"alert alert-success\">WebDavUrls: <br />" + urls.join("<br />") + "</p>";
        }
        BootstrapDialog.alert({
            title: 'Example',
            message: msg,
            type: BootstrapDialog.TYPE_PRIMARY,
            closable: true,
            draggable: true
        });
        DeSelectAllinList();
        return false;
    });
});
