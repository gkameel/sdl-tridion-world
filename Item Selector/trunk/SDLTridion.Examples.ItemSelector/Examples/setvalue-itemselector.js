﻿// this function gets called from the itemSelector PopOver (called from loaded iframe)
function SetValueFromItemSelector(value, fieldId) {
    console.log("SetValueFromItemSelector called with parameters (value = '" + value + "', fieldId = '" + fieldId + "')");

    var val = value;

    if (value.uri != undefined) {
        val = value.uri;
        console.log("value.requestedValue: " + value.requestedValue);
    }
    if (value.requestedValue != undefined) {
        val = value.requestedValue;
    }

    var target = $('#' + fieldId);

    if (target.prop("tagName").toUpperCase() == "SELECT") {
        target.append($("<option data-toggle=\"tooltip\"></option>").attr("value", val).attr("title", val).text(val));
    }

    else if ((target.prop("tagName").toUpperCase() == "INPUT")) {
        if (target.attr("type").toUpperCase() == "TEXT" || target.attr("type").toUpperCase() == "HIDDEN") {
            target.val(val);
        }
    }

    else {
        target.text(val);
    }

    // close all popovers
    $('[data-toggle="popover"]').popover("destroy");
    ActivateToolTips();
}
