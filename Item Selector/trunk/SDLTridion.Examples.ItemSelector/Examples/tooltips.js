﻿function ActivateToolTips() {
    $('[data-toggle="tooltip"][title!=""]').tooltip({ trigger: "manual", placement: 'auto', container: 'body', html: 'true' })
        .on("mouseenter", function () {
            var that = this;
            $(this).tooltip("show");
            $(".tooltip").on("mouseleave", function () {
                $(that).tooltip('hide');
            });
        }).on("mouseleave", function () {
            var that = this;
            setTimeout(function () {
                if (!$(".tooltip:hover").length) {
                    $(that).tooltip("hide");
                }
            }, 300);
        });
}
