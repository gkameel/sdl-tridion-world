﻿function EnableItemSelectors() {
    // enable all itemSelectors on the page
    $("[data-itemselector-for]").click(function (event) {
        $('.tooltip').hide(); // hide tooltips
        ShowItemSelector(this);
        return false;
    });
}

// enable itemSelector for this element
function ShowItemSelector(e) {
    var href = e.href; // href on the element
    var fieldId = $(e).data("itemselector-for"); // ID of element to insert value in the HTML DOM (mandatory)
    var start = $(e).data("itemselector-start"); // start Uri (Publication uri, default: none)
    var types = $(e).data("itemselector-types"); // allowed itemtypes (int)
    var returnValue = $(e).data("itemselector-returnvalue"); // type of value expected from ItemSelector (uri, id, path, webdavurl)
    var height = $(e).data("itemselector-height"); // height of ItemSelector Popover
    if (height == undefined || height == "") {
        height = 400;
    }

    var url = '#urlMissing';
    if (href != undefined && href != "") {
        url = href + '?';
        if (fieldId != undefined && fieldId != "") {
            url = url + 'fieldId=' + fieldId + '&';
        }
        if (start != undefined && start != "") {
            url = url + 'start=' + start + '&';
        }
        if (types != undefined && types != "") {
            url = url + 'types=' + types + '&';
        }
        if (returnValue != undefined && returnValue != "") {
            url = url + 'returnValue=' + returnValue + '&';
        }
    }
    console.log("ItemSelector url:" + url);
    $(e).popover({
        content: '<iframe id="selectorframe" src="' + url + '" style="display:block;" height="' + height + '", width="400" frameBorder="0"></iframe>',
        html: true
    }).popover('toggle');
}
